<?php
class SubscriberUsage extends AppModel {

	var $name = 'SubscriberUsage';
	var $useTable = 'Subscriber_Usage';
	var $primaryKey = 'SUBSCRIBER_ID';
	var $validate = array(
		'TIMESTAMP_MILLI' => array('numeric'),
		'RDR_TYPE' => array('numeric'),
		'SUBSCRIBER_ID' => array('alphanumeric'),
		'PACKAGE_ID' => array('numeric'),
		'SERVICE_USAGE_COUNTER_ID' => array('numeric'),
		'BREACH_STATE' => array('boolean'),
		'REASON' => array('numeric'),
		'CONFIGURED_DURATION' => array('numeric'),
		'DURATION' => array('numeric'),
		'END_TIME' => array('numeric'),
		'UPSTREAM_VOLUME' => array('numeric'),
		'DOWNSTREAM_VOLUME' => array('numeric'),
		'SESSIONS' => array('numeric'),
		'SECONDS' => array('numeric'),
		'UP_VLINK' => array('numeric'),
		'DOWN_VLINK' => array('numeric'),
		'csvFile' => array('alphanumeric')
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
/*	var $belongsTo = array(
		'Package' => array(
			'className' => 'Package',
			'foreignKey' => 'PACKAGE_ID',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
*/
}
?>