<?php
class Package extends AppModel {

	var $name = 'Package';
	var $useTable = 'Packages';
	#var $primaryKey = 'PACKAGE_ID';
	var $validate = array(
		'NAME' => array('notempty'),
		'PACKAGE_ID' => array('numeric'),
		'BUCKET_SIZE' => array('numeric'),
		'DOSAGE_SIZE' => array('numeric'),
		'AGGREGATION_PERIOD' => array('rule' => array('inList', array('minutes','hourly','daily','weekly','monthly'))), 
		#'AGGREGATION_PERIOD' => array('inList', array('minutes','hourly','daily','weekly','monthly')),	# doesn't work
		'DAY_OF_MONTH' => array('numeric'),
		#'DAY_OF_WEEK' => array('inlist', array('sunday','monday','tuesday','wednesday','thursday','friday','saturday'))
	);


}
?>