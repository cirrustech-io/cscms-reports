<?php
class RealtimeSubscriberUsage extends AppModel {

	var $name = 'RealtimeSubscriberUsage';
	var $useTable = 'Realtime_Subscriber_Usage';
	var $primaryKey = 'TIMESTAMP_MILLI';
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'Package' => array(
			'className' => 'Package',
			'foreignKey' => 'PACKAGE_ID',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	
}
?>