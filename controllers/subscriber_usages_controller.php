<?php
class SubscriberUsagesController extends AppController {

	var $name = 'SubscriberUsages';
	var $helpers = array('Html', 'Form');

	function index() {
		$this->SubscriberUsage->recursive = 0;
		$this->set('subscriberUsages', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid SubscriberUsage.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('subscriberUsage', $this->SubscriberUsage->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->SubscriberUsage->create();
			if ($this->SubscriberUsage->save($this->data)) {
				$this->Session->setFlash(__('The SubscriberUsage has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The SubscriberUsage could not be saved. Please, try again.', true));
			}
		}
		$packages = $this->SubscriberUsage->Package->find('list');
		$this->set(compact('packages'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid SubscriberUsage', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->SubscriberUsage->save($this->data)) {
				$this->Session->setFlash(__('The SubscriberUsage has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The SubscriberUsage could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->SubscriberUsage->read(null, $id);
		}
		$packages = $this->SubscriberUsage->Package->find('list');
		$this->set(compact('packages'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for SubscriberUsage', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->SubscriberUsage->del($id)) {
			$this->Session->setFlash(__('SubscriberUsage deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
	
	function volume( $date_target = null ) {
		
		# The target date is the first day of the month for which the report is required
		# for December 2010 the value is 2010-12-01
		$date_target = '2010-12-01';
		
		# Create a volume usage summary
		$result = array();
		if( $date_target == null  )
		{
			# Set default range as last month
			$date_begin = date('Y-m', strtotime("last month")) . "-01";
			$date_end = date('Y-m') . "-01";
		}
		else
		{
			$date_begin = $date_target;
			$date_end = date('Y-m', strtotime('+1 month', strtotime($date_begin))) . '-01';
		} # End if-else
		

		#
		# Query database for subscriber usage information
		#
		$subscribers = $this->SubscriberUsage->find(
			'all',
			array(
				'conditions' => array(
					'SubscriberUsage.TIMESTAMP_MILLI >=' => strtotime($date_begin)*1000,
					'SubscriberUsage.TIMESTAMP_MILLI <' => strtotime($date_end)*1000,
				), //array of conditions
				'recursive' => 0, //int
				'fields' => array(
					'SubscriberUsage.SUBSCRIBER_ID',
					'SUM(SubscriberUsage.DOWNSTREAM_VOLUME)/1000 AS DOWNLOADED',
					'SUM(SubscriberUsage.UPSTREAM_VOLUME)/1000 AS UPLOADED',
					'(SUM(SubscriberUsage.DOWNSTREAM_VOLUME)/1000 + SUM(SubscriberUsage.UPSTREAM_VOLUME)/1000)  AS COMBINED',
					'FROM_UNIXTIME(MAX(TIMESTAMP_MILLI)/1000) AS LAST_RECORD',
				), //array of field names
				'order' => array('SubscriberUsage.SUBSCRIBER_ID ASC'), //string or array defining order
				'joins' => array(), // array of arrays defining join operations
				'group' => array('SubscriberUsage.SUBSCRIBER_ID'), //fields to GROUP BY
			)
		);
		
		$this->set( 'subscriberUsages', $subscribers );

		#debug( $date_begin );
		#debug( $date_end );	
		#debug( $subscribers );
	}

}
?>