<?php
class RealtimeSubscriberUsagesController extends AppController {

	var $name = 'RealtimeSubscriberUsages';
	var $helpers = array('Html', 'Form');

	function index() {
		$this->RealtimeSubscriberUsage->recursive = 0;
		$this->set('realtimeSubscriberUsages', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid RealtimeSubscriberUsage.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('realtimeSubscriberUsage', $this->RealtimeSubscriberUsage->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->RealtimeSubscriberUsage->create();
			if ($this->RealtimeSubscriberUsage->save($this->data)) {
				$this->Session->setFlash(__('The RealtimeSubscriberUsage has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The RealtimeSubscriberUsage could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid RealtimeSubscriberUsage', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->RealtimeSubscriberUsage->save($this->data)) {
				$this->Session->setFlash(__('The RealtimeSubscriberUsage has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The RealtimeSubscriberUsage could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->RealtimeSubscriberUsage->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for RealtimeSubscriberUsage', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->RealtimeSubscriberUsage->del($id)) {
			$this->Session->setFlash(__('RealtimeSubscriberUsage deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
	
	function volume( $date_target = null ) {
		#debug($date_target);
		# The target date is the first day of the month for which the report is required
		# for December 2010 the value is 2010-12-01
		#$date_target = '2010-12-01';
		
		# Create a volume usage summary
		$result = array();
		if( $date_target == null  )
		{
			# Set default range as this month
			$date_begin = date('Y-m') . "-01";
			$date_end = date('Y-m', strtotime("next month")) . "-01";
		}
		else
		{
			$date_begin = $date_target;
			$date_end = date('Y-m', strtotime('+1 month', strtotime($date_begin))) . '-01';
		} # End if-else
		

		#
		# Query database for subscriber usage information
		#
		$subscribers = $this->RealtimeSubscriberUsage->find(
			'all',
			array(
				'conditions' => array(
					'RealtimeSubscriberUsage.TIMESTAMP_MILLI >=' => strtotime($date_begin)*1000,
					'RealtimeSubscriberUsage.TIMESTAMP_MILLI <' => strtotime($date_end)*1000,
					#'RealtimeSubscriberUsage.SUBSCRIBER_ID' => 'YounisH'
				), //array of conditions
				'recursive' => 0, //int
				'fields' => array(
					'RealtimeSubscriberUsage.SUBSCRIBER_ID',
					'SUM(RealtimeSubscriberUsage.DOWNSTREAM_VOLUME)/1000 AS DOWNLOADED',
					'SUM(RealtimeSubscriberUsage.UPSTREAM_VOLUME)/1000 AS UPLOADED',
					'(SUM(RealtimeSubscriberUsage.DOWNSTREAM_VOLUME)/1000 + SUM(RealtimeSubscriberUsage.UPSTREAM_VOLUME)/1000)  AS COMBINED',
					'FROM_UNIXTIME(MAX(TIMESTAMP_MILLI)/1000) AS LAST_RECORD',
					'RealtimeSubscriberUsage.PACKAGE_ID'
				), //array of field names
				'order' => array('RealtimeSubscriberUsage.SUBSCRIBER_ID ASC'), //string or array defining order
				'joins' => array(), // array of arrays defining join operations
				'group' => array('RealtimeSubscriberUsage.SUBSCRIBER_ID'), //fields to GROUP BY
			)
		);
		
		$this->set( 'usages', $subscribers );
		
		# Generate dates for drop-down filtering
		# $this->RealtimeSubscriberUsage->generateDates();
		$date_filters = $this->generateDates( $date_begin );
		$this->set( 'dates', $date_filters );
		# debug( $date_filters );
		# debug( $date_begin );
		# debug( $date_end );	
		# debug( $subscribers );
		
		# vvv Works vvv
		#$packages = $this->RealtimeSubscriberUsage->Package->find('all');
		#debug( $packages );
		# ^^^ Works ^^^
		
		
		$packages = $this->RealtimeSubscriberUsage->Package->find(
			'list',
			array(
				'recursive' => 0, //int
				'fields' => array(
					'Package.NAME',
					'Package.BUCKET_SIZE',
					'Package.PACKAGE_ID',
			#'Package.AGGREGATION_PERIOD',
				), //array of field names
			)
		);
		
		#debug( $packages );
		$this->set( 'packages', $packages );
		
		
		
		
	}
	
	function generateDates( $date_selected = null ) {
		$date_target = array();
		$keep_generating = true;
		$date_thisMonth = $date_current = date('Y-m')."-01";
		$date_lastmonth = date('Y-m', strtotime('-1 month'))."-01";
		$date_terminate = date('Y-m', strtotime('-1 year -1 month'))."-01";
		
		# Set earliest date to earliest records if $date_terminate is too far in the past
		$date_earliestRecords = '2010-06-01'; # verified by sql queries against database
		if( strtotime($date_terminate) < strtotime($date_earliestRecords) )
			$date_terminate = $date_earliestRecords;
		
		
		#echo $date_terminate;
		
		while( $keep_generating )
		{
			$date_target[$date_current] = date('Y - F', strtotime($date_current));
			$date_current = date('Y-m', strtotime('-1 month', strtotime($date_current))).'-01';
			$keep_generating = ($date_current==$date_terminate)?false:true;
		}
		/*#echo '<pre>' . print_r( $date_target,1 ) . '</pre>';
		echo form_open('usage/volume', array('class'=>'date_select'));
		echo form_dropdown('date_target', $date_target, $date_lastmonth);
		echo form_submit('submit','Get Report');
		echo form_close();*/

		#debug( $date_target );
		#debug($date_thisMonth);
		#debug($date_lastmonth);
		return array( 
			'selected' => $date_selected,
			'thisMonth' => $date_thisMonth,
			'lastMonth' => $date_lastmonth,
			'list' => $date_target
		);
	}
	
}
?>