<div class="realtimeSubscriberUsages view">
<h2><?php  __('RealtimeSubscriberUsage');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('TIMESTAMP MILLI'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $realtimeSubscriberUsage['RealtimeSubscriberUsage']['TIMESTAMP_MILLI']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('RDR TYPE'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $realtimeSubscriberUsage['RealtimeSubscriberUsage']['RDR_TYPE']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('SUBSCRIBER ID'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $realtimeSubscriberUsage['RealtimeSubscriberUsage']['SUBSCRIBER_ID']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('PACKAGE ID'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $realtimeSubscriberUsage['RealtimeSubscriberUsage']['PACKAGE_ID']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('SERVICE USAGE COUNTER ID'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $realtimeSubscriberUsage['RealtimeSubscriberUsage']['SERVICE_USAGE_COUNTER_ID']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('AGGREGATION OBJECT ID'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $realtimeSubscriberUsage['RealtimeSubscriberUsage']['AGGREGATION_OBJECT_ID']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('BREACH STATE'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $realtimeSubscriberUsage['RealtimeSubscriberUsage']['BREACH_STATE']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('REASON'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $realtimeSubscriberUsage['RealtimeSubscriberUsage']['REASON']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('CONFIGURED DURATION'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $realtimeSubscriberUsage['RealtimeSubscriberUsage']['CONFIGURED_DURATION']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('DURATION'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $realtimeSubscriberUsage['RealtimeSubscriberUsage']['DURATION']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('END TIME'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $realtimeSubscriberUsage['RealtimeSubscriberUsage']['END_TIME']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('UPSTREAM VOLUME'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $realtimeSubscriberUsage['RealtimeSubscriberUsage']['UPSTREAM_VOLUME']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('DOWNSTREAM VOLUME'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $realtimeSubscriberUsage['RealtimeSubscriberUsage']['DOWNSTREAM_VOLUME']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('SESSIONS'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $realtimeSubscriberUsage['RealtimeSubscriberUsage']['SESSIONS']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('SECONDS'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $realtimeSubscriberUsage['RealtimeSubscriberUsage']['SECONDS']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('CsvFile'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $realtimeSubscriberUsage['RealtimeSubscriberUsage']['csvFile']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $realtimeSubscriberUsage['RealtimeSubscriberUsage']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $realtimeSubscriberUsage['RealtimeSubscriberUsage']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit RealtimeSubscriberUsage', true), array('action'=>'edit', $realtimeSubscriberUsage['RealtimeSubscriberUsage']['TIMESTAMP_MILLI'])); ?> </li>
		<li><?php echo $html->link(__('Delete RealtimeSubscriberUsage', true), array('action'=>'delete', $realtimeSubscriberUsage['RealtimeSubscriberUsage']['TIMESTAMP_MILLI']), null, sprintf(__('Are you sure you want to delete # %s?', true), $realtimeSubscriberUsage['RealtimeSubscriberUsage']['TIMESTAMP_MILLI'])); ?> </li>
		<li><?php echo $html->link(__('List RealtimeSubscriberUsages', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New RealtimeSubscriberUsage', true), array('action'=>'add')); ?> </li>
	</ul>
</div>
