<div class="realtimeSubscriberUsages form">
<?php echo $form->create('RealtimeSubscriberUsage');?>
	<fieldset>
 		<legend><?php __('Edit RealtimeSubscriberUsage');?></legend>
	<?php
		echo $form->input('TIMESTAMP_MILLI');
		echo $form->input('RDR_TYPE');
		echo $form->input('SUBSCRIBER_ID');
		echo $form->input('PACKAGE_ID');
		echo $form->input('SERVICE_USAGE_COUNTER_ID');
		echo $form->input('AGGREGATION_OBJECT_ID');
		echo $form->input('BREACH_STATE');
		echo $form->input('REASON');
		echo $form->input('CONFIGURED_DURATION');
		echo $form->input('DURATION');
		echo $form->input('END_TIME');
		echo $form->input('UPSTREAM_VOLUME');
		echo $form->input('DOWNSTREAM_VOLUME');
		echo $form->input('SESSIONS');
		echo $form->input('SECONDS');
		echo $form->input('csvFile');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('RealtimeSubscriberUsage.TIMESTAMP_MILLI')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('RealtimeSubscriberUsage.TIMESTAMP_MILLI'))); ?></li>
		<li><?php echo $html->link(__('List RealtimeSubscriberUsages', true), array('action'=>'index'));?></li>
	</ul>
</div>
