<div class="subscriberUsages index">
<h2>Data Volume Usage: <?php echo date('F Y', strtotime( $dates['selected'] ) );?></h2>


<?php 
	/*echo $form->create('RealtimeSubscriberUsage', array( 'action'=>'volume', 'type'=>'get' ) );
	echo $form->select('date', $dates['list'], array('selected'=>$dates['selected']) );
	#echo $form->
	echo $form->end('Submit');*/ 
?>

<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo 'Subscriber ID';?></th>
	<th><?php echo 'Package';?></th>
	<th><?php echo 'Allowed';?></th>
	<!--<th><?php echo 'Downstream Volume';?></th>
	<th><?php echo 'Upstream Volume';?></th>
	--><th><?php echo 'Transfer Volume';?></th>
	<th><?php echo 'Last Record';?></th>
</tr>
<?php
$i = 0;
foreach ($usages as $usage):
	#debug( $subscriberUsage );
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $usage['RealtimeSubscriberUsage']['SUBSCRIBER_ID']; ?>
		</td>
		<td>
			<?php echo (isset($packages[$usage['RealtimeSubscriberUsage']['PACKAGE_ID']]))?key($packages[$usage['RealtimeSubscriberUsage']['PACKAGE_ID']])." [".$usage['RealtimeSubscriberUsage']['PACKAGE_ID']."]":"N/A"; ?>
		</td>
		<td>
			<?php echo (isset($packages[$usage['RealtimeSubscriberUsage']['PACKAGE_ID']]))?number_format($packages[$usage['RealtimeSubscriberUsage']['PACKAGE_ID']][key($packages[$usage['RealtimeSubscriberUsage']['PACKAGE_ID']])]/1000,1)." MB":"N/A"; ?>
		</td>
		<!--<td>
			<?php echo number_format( $usage[0]['DOWNLOADED'] , 1 ); ?> MB
		</td>
		<td>
			<?php echo number_format( $usage[0]['UPLOADED'] , 1 ); ?> MB
		</td>
		--><td>
			<?php echo number_format( $usage[0]['COMBINED'] , 1 ); ?> MB
		</td>
		<td>
			<?php echo $usage[0]['LAST_RECORD']; ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div>
<ul>
	<?php foreach( $dates['list'] as $date => $title ):?>
	<li><?php echo $html->link(date('M Y', strtotime($date)), array('action'=>'volume', $date)); ?></li>
	<?php endforeach; ?>
</ul>
</div>