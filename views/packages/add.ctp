<div class="packages form">
<?php echo $form->create('Package');?>
	<fieldset>
 		<legend><?php __('Add Package');?></legend>
	<?php
		echo $form->input('NAME', array('label'=>'Package Name'));
		echo $form->input('PACKAGE_ID', array('label'=>'CSCMS Package ID'));
		echo $form->input('BUCKET_SIZE', array('label'=>'Bucket Size'));
		echo $form->input('DOSAGE_SIZE', array('label'=>'Dosage Size'));
		echo $form->input('AGGREGATION_PERIOD', 
			array(
				'label'	=>	'Aggregation Period', 
				'type'	=>	'select', 
				'options'	=>	array(
					'minutes' => 'minutes',
					'hourly' => 'hourly',
					'daily' => 'daily',
					'weekly' => 'weekly',
					'monthly' => 'monthly'
				), 
				'selected'=>'monthly'
			)
		); # done w/ input
		#echo $form->input('AGGREGATION_MINUTES', array('label'=>'Aggregation Minutes'));
		echo $form->input('DAY_OF_MONTH', array('label'=>'Day of Month'));
		#echo $form->input('DAY_OF_WEEK', array('label'=>'Day of Week'));
		#echo $form->input('TIME_OF_DAY', array('label'=>'Time of Day'));
		#echo $form->input('GAP', array('label'=>'Gap'));
		#echo $form->input('PENALTY_PERIOD', array('label'=>'Penalty Period'));
		#echo $form->input('PENALTY_PROFILE', array('label'=>'Penalty Profile'));
		#echo $form->input('POST_PENALTY_THRESHOLD', array('label'=>'Post-Penalty Threshold'));
		#echo $form->input('POST_PENALTY_PROFILE', array('label'=>'Post-Penalty Profile'));
		#echo $form->input('SLICE_PERIOD', array('label'=>'Slice Period'));
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Packages', true), array('action'=>'index'));?></li>
	</ul>
</div>
