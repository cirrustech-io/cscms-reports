<div class="packages index">
<h2><?php __('Packages');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('NAME');?></th>
	<th><?php echo $paginator->sort('PACKAGE_ID');?></th>
	<th><?php echo $paginator->sort('BUCKET_SIZE');?></th>
	<th><?php echo $paginator->sort('DOSAGE_SIZE');?></th>
	<th><?php echo $paginator->sort('AGGREGATION_PERIOD');?></th>
	<th><?php echo $paginator->sort('DAY_OF_MONTH');?></th>
	<!-- <th><?php echo $paginator->sort('AGGREGATION_MINUTES');?></th>
	<th><?php echo $paginator->sort('DAY_OF_WEEK');?></th>
	<th><?php echo $paginator->sort('TIME_OF_DAY');?></th>
	<th><?php echo $paginator->sort('GAP');?></th>
	<th><?php echo $paginator->sort('PENALTY_PERIOD');?></th>
	<th><?php echo $paginator->sort('PENALTY_PROFILE');?></th>
	<th><?php echo $paginator->sort('POST_PENALTY_THRESHOLD');?></th>
	<th><?php echo $paginator->sort('POST_PENALTY_PROFILE');?></th>
	<th><?php echo $paginator->sort('SLICE_PERIOD');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('modified');?></th>
	 -->
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($packages as $package):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $package['Package']['id']; ?>
		</td>
		<td>
			<?php echo $package['Package']['NAME']; ?>
		</td>
		<td>
			<?php echo $package['Package']['PACKAGE_ID']; ?>
		</td>
		<td>
			<?php echo $package['Package']['BUCKET_SIZE']/1000/1000; ?> GB
		</td>
		<td>
			<?php echo $package['Package']['DOSAGE_SIZE']/1000; ?> MB
		</td>
		<td>
			<?php echo $package['Package']['AGGREGATION_PERIOD']; ?>
		</td>
		<td>
			<?php echo $package['Package']['DAY_OF_MONTH']; ?>
		</td>
		<!-- 
		<td>
			<?php echo $package['Package']['AGGREGATION_MINUTES']; ?>
		</td>
		<td>
			<?php echo $package['Package']['DAY_OF_WEEK']; ?>
		</td>
		<td>
			<?php echo $package['Package']['TIME_OF_DAY']; ?>
		</td>
		<td>
			<?php echo $package['Package']['GAP']; ?>
		</td>
		<td>
			<?php echo $package['Package']['PENALTY_PERIOD']; ?>
		</td>
		<td>
			<?php echo $package['Package']['PENALTY_PROFILE']; ?>
		</td>
		<td>
			<?php echo $package['Package']['POST_PENALTY_THRESHOLD']; ?>
		</td>
		<td>
			<?php echo $package['Package']['POST_PENALTY_PROFILE']; ?>
		</td>
		<td>
			<?php echo $package['Package']['SLICE_PERIOD']; ?>
		</td>
		<td>
			<?php echo $package['Package']['created']; ?>
		</td>
		<td>
			<?php echo $package['Package']['modified']; ?>
		</td>
		 -->
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action'=>'view', $package['Package']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $package['Package']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $package['Package']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $package['Package']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Package', true), array('action'=>'add')); ?></li>
	</ul>
</div>
