<div class="packages form">
<?php echo $form->create('Package');?>
	<fieldset>
 		<legend><?php __('Edit Package');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('NAME', array('label'=>'Package Name'));
		echo $form->input('PACKAGE_ID', array('label'=>'CSCMS Package ID'));
		echo $form->input('BUCKET_SIZE', array('label'=>'Bucket Size'));
		echo $form->input('DOSAGE_SIZE', array('label'=>'Dosage Size'));
		echo $form->input('AGGREGATION_PERIOD', 
			array(
				'label'	=>	'Aggregation Period', 
				'type'	=>	'select', 
				'options'	=>	array(
					'minutes' => 'minutes',
					'hourly' => 'hourly',
					'daily' => 'daily',
					'weekly' => 'weekly',
					'monthly' => 'monthly'
				), 
				'selected'=>'monthly'
			)
		);
		#echo $form->input('AGGREGATION_MINUTES');
		echo $form->input('DAY_OF_MONTH', array('label'=>'Day of Month'));
		#echo $form->input('DAY_OF_WEEK');
		#echo $form->input('TIME_OF_DAY');
		#echo $form->input('GAP');
		#echo $form->input('PENALTY_PERIOD');
		#echo $form->input('PENALTY_PROFILE');
		#echo $form->input('POST_PENALTY_THRESHOLD');
		#echo $form->input('POST_PENALTY_PROFILE');
		#echo $form->input('SLICE_PERIOD');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('Package.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Package.id'))); ?></li>
		<li><?php echo $html->link(__('List Packages', true), array('action'=>'index'));?></li>
	</ul>
</div>
