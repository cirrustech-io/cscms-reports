<div class="subscriberUsages index">
<h2><?php __('SubscriberUsages');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('TIMESTAMP_MILLI');?></th>
	<th><?php echo $paginator->sort('RDR_TYPE');?></th>
	<th><?php echo $paginator->sort('SUBSCRIBER_ID');?></th>
	<th><?php echo $paginator->sort('PACKAGE_ID');?></th>
	<th><?php echo $paginator->sort('SERVICE_USAGE_COUNTER_ID');?></th>
	<th><?php echo $paginator->sort('BREACH_STATE');?></th>
	<th><?php echo $paginator->sort('REASON');?></th>
	<th><?php echo $paginator->sort('CONFIGURED_DURATION');?></th>
	<th><?php echo $paginator->sort('DURATION');?></th>
	<th><?php echo $paginator->sort('END_TIME');?></th>
	<th><?php echo $paginator->sort('UPSTREAM_VOLUME');?></th>
	<th><?php echo $paginator->sort('DOWNSTREAM_VOLUME');?></th>
	<th><?php echo $paginator->sort('SESSIONS');?></th>
	<th><?php echo $paginator->sort('SECONDS');?></th>
	<th><?php echo $paginator->sort('UP_VLINK');?></th>
	<th><?php echo $paginator->sort('DOWN_VLINK');?></th>
	<th><?php echo $paginator->sort('csvFile');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('modified');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($subscriberUsages as $subscriberUsage):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['TIMESTAMP_MILLI']; ?>
		</td>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['RDR_TYPE']; ?>
		</td>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['SUBSCRIBER_ID']; ?>
		</td>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['PACKAGE_ID']; ?>
		</td>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['SERVICE_USAGE_COUNTER_ID']; ?>
		</td>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['BREACH_STATE']; ?>
		</td>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['REASON']; ?>
		</td>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['CONFIGURED_DURATION']; ?>
		</td>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['DURATION']; ?>
		</td>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['END_TIME']; ?>
		</td>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['UPSTREAM_VOLUME']; ?>
		</td>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['DOWNSTREAM_VOLUME']; ?>
		</td>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['SESSIONS']; ?>
		</td>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['SECONDS']; ?>
		</td>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['UP_VLINK']; ?>
		</td>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['DOWN_VLINK']; ?>
		</td>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['csvFile']; ?>
		</td>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['created']; ?>
		</td>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['modified']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action'=>'view', $subscriberUsage['SubscriberUsage']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $subscriberUsage['SubscriberUsage']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $subscriberUsage['SubscriberUsage']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $subscriberUsage['SubscriberUsage']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New SubscriberUsage', true), array('action'=>'add')); ?></li>
	</ul>
</div>
