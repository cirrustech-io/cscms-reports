<div class="subscriberUsages index">
<h2><?php __('SubscriberUsages');?></h2>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo 'Subscriber ID';?></th>
	<th><?php echo 'Downstream Volume';?></th>
	<th><?php echo 'Upstream Volume';?></th>
	<th><?php echo 'Combined Volume';?></th>
	<th><?php echo 'Last Record';?></th>
</tr>
<?php
$i = 0;
foreach ($subscriberUsages as $subscriberUsage):
	#debug( $subscriberUsage );
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $subscriberUsage['SubscriberUsage']['SUBSCRIBER_ID']; ?>
		</td>
		<td>
			<?php echo number_format( $subscriberUsage[0]['DOWNLOADED'] , 1 ); ?> MB
		</td>
		<td>
			<?php echo number_format( $subscriberUsage[0]['UPLOADED'] , 1 ); ?> MB
		</td>
		<td>
			<?php echo number_format( $subscriberUsage[0]['COMBINED'] , 1 ); ?> MB
		</td>
		<td>
			<?php echo $subscriberUsage[0]['LAST_RECORD']; ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
